﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorriqForum.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public DateTime SignUpDate { get; set; }
    }
}