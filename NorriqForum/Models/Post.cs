﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorriqForum.Models
{
    public class Post
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int PostId { get; set; }
        public User Owner { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public List<Answer> Answers { get; set; }
    }
}