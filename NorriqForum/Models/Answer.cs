﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NorriqForum.Models
{
    public class Answer
    {
        public int AnswerId { get; set; }
        public int PostId { get; set; }
        public User User { get; set; }
        public string Content { get; set; }
        public bool Approved { get; set; }
        public DateTime Created{ get; set; }
        public DateTime ? Updated { get; set; }
    
    }
}