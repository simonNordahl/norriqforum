﻿
var app = angular.module('forum', [])

app.factory('resource', function ($http) {
            return {

                PostAnswer: function (answer, postId) {
                    return $http.post('/api/forum/PostAnswer?content=' + answer + '&postId=' + postId);
                },
                GetAnswers: function(postId)
                {
                    return $http.get('/api/forum/GetAnswers?postId=' + postId);
                    
                },
                GetUser: function()
                {
                    return $http.get('/api/forum/GetUser');
                    
                },
                MarkAsAnswer : function(id)
                {
                    return $http.post('/api/forum/MarkAsApproved?answerId=' + id);
                }
            }
})