﻿using NorriqForum.DB;
using NorriqForum.Models;
using NorriqForum.Models.FormModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace NorriqForum.Controllers
{

    public class PostController : Controller
    {
        private NFE db = new NFE();
        private int UserId()
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var email = prinicpal.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
            return db.Users.First(u => u.Email == email).UserID;
        }

        public ActionResult CreatePost()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreatePost(PostModel model)
        {

            if (ModelState.IsValid)
            {
                var post = new DB.Posts();
                post.Content = model.Content;
                post.Title = model.Title;
                post.OwnerId = UserId();
                post.Created = DateTime.Now;

                db.Posts.Add(post);
                db.SaveChanges();




                return View();
            }

            ModelState.AddModelError("", "Invalid input");
            return View();
        }

        [HttpGet]
        public ActionResult ShowPost()
        {
            var id = Convert.ToInt32(RouteData.Values["id"] + Request.Url.Query);
            var post = (from p in db.Posts
                        where p.PostId == id
                        select new Post()
                        {
                            Title = p.Title,
                            Content = p.Content,
                            Created = p.Created,
                            Updated = p.Updated,
                            PostId = p.PostId,
                            Owner = (from u in db.Users
                                     where u.UserID == p.OwnerId
                                     select new User()
                                     {
                                         Email = u.Email,
                                         Username = u.Username

                                     }).FirstOrDefault()
                        }).FirstOrDefault();

            post.Answers = (from a in db.Answers
                            where a.PostId == id
                            select new Answer
                            {
                                Content = a.Content,
                                AnswerId = a.AnswerId,
                                Approved = a.Approved,
                                PostId = a.PostId,
                                User = (from u in db.Users
                                        where u.UserID == a.UserId
                                        select new User()
                                        {
                                            Email = u.Email,
                                            Username = u.Username

                                        }).FirstOrDefault(),
                            }).ToList();
            if (post.Answers.Count == 0)
            {
                post.Answers.Add(new Answer());
            }

            return View(post);
        }
        [HttpPost]
        public ActionResult ShowPost(Post model, int id)
        {
            var post = db.Posts.FirstOrDefault(p => p.PostId == id);
            post.Content = model.Content;
            post.Title = model.Title;
            post.Updated = DateTime.Now;

            db.SaveChanges();
            return ShowPost();

        }
    }
}