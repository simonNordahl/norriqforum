﻿using NorriqForum.DB;
using NorriqForum.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Http.Owin;


namespace NorriqForum.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {

            private DB.NFE db = new NFE ();
            public ActionResult Login()
            {
                return View(new LoginModel());
            }
            [HttpPost]
            public ActionResult Login(LoginModel model)
            {
                var dbUser = db.Users.FirstOrDefault(u => u.Email == model.Email);
                //Check if the given username excists and the login form is valid.
                if (dbUser != null && ModelState.IsValid)
                {
                    var users = db.Users.FindAsync(model.Email, model.Password);
                    var encryptMethod = dbUser.EncryptionMethod;
                    //Encodes the given password with the same method as the password for the user in the database.
                    var password = FormsAuthentication.HashPasswordForStoringInConfigFile(model.Password, encryptMethod);
                    //Checks if the password and username combination is valid.
                    if (db.Users.Any(user => user.Email == model.Email && user.Password == password))
                    {
                        //Sets the identity of the user
                        var user = db.Users.First(u => u.Email == model.Email && u.Password == password);
                        var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Email, user.Email)},

                        "ApplicationCookie");

                        var ctx = Request.GetOwinContext();
                        var authManager = ctx.Authentication;

                        authManager.SignIn(identity);

                        return Redirect(GetRedirectUrl(model.ReturnUrl));
                    }
                }
                //Default form error on failed logins
                ModelState.AddModelError("", "Invalid email or password");
                return View(new LoginModel());

            }
            public ActionResult Logout()
            {
                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignOut("ApplicationCookie");
                return RedirectToAction("index", "home");
            }
            [HttpGet]
            public ActionResult Register()
            {
                return View();
            }
            [HttpPost]
            public ActionResult Register(RegisterModel model)
            {

                string encryptionMethod = "SHA1";
                var excist = db.Users.Any(u => u.Username == model.Username || u.Email == model.Email);
                
                if (!ModelState.IsValid || excist)
                {
                    if(excist)
                    {
                        ModelState.AddModelError("", "That username/email does allready excists");
                    }
                    return View();
                }

                var user = new DB.Users();
                user.Username = model.Username;
                user.Email = model.Email;
                user.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(model.Password, encryptionMethod);
                user.SignupDate = DateTime.Now.Date;
                user.EncryptionMethod = encryptionMethod;

                db.Users.Add(user);
                db.SaveChanges();

                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, user.Username),
                    new Claim(ClaimTypes.Email, user.Email)},

    "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);
                return RedirectToAction("index", "home");

            }
            private string GetRedirectUrl(string returnUrl)
            {
                if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                {
                    return Url.Action("index", "home");
                }

                return returnUrl;
            }

        }
    }
