﻿using NorriqForum.DB;
using NorriqForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NorriqForum.Controllers
{
      [AllowAnonymous]
    public class HomeController : Controller
    {
        private DB.NFE db = new NFE();
        public ActionResult Index()
        {

            List<Post> threads = (from p in db.Posts
                                  orderby p.Created descending
                                  select new Post()
                                  {
                                      Title = p.Title,
                                      Content = p.Content,
                                      Created = p.Created,
                                      Updated = p.Updated,
                                      PostId = p.PostId,
                                      Answers = (from a in db.Answers where a.PostId == p.PostId
                                                     select new Answer{
                                                     AnswerId = a.AnswerId,
                                                     Approved = a.Approved,
                                                     Created = a.Created,
                                                     Content = a.Content,
                                                     PostId = a.PostId,
                                                     Updated = a.Updated,
                                                     User = (from u in db.Users
                                               where u.UserID == a.UserId
                                               select new User()
                                               {
                                                   Email = u.Email,
                                                   Username = u.Username
                                               }).FirstOrDefault()
                                                     

                                                     }).ToList(),


                                      Owner = (from u in db.Users
                                               where u.UserID == p.OwnerId
                                               select new User()
                                               {
                                                   Email = u.Email,
                                                   Username = u.Username
                                               }).FirstOrDefault()
                                  }).ToList();


            return View(threads);
        }
    }
}