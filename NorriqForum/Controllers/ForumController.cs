﻿using NorriqForum.DB;
using NorriqForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;

namespace NorriqForum.Controllers
{
  
    public class ForumController : ApiController
    {
        private NFE db = new NFE();
        [HttpGet]
        public List<Answer> GetAnswers (int postId)
        {
          
            List<Answer> answers = db.Answers.Where(a => a.PostId == postId).Select(s => new Answer
            {
                AnswerId = s.AnswerId,
                Approved = s.Approved,
                Content = s.Content,
                PostId = s.PostId,
                Created = s.Created,
                Updated = s.Updated,
                User = db.Users.Where(u => u.UserID == s.UserId).Select(u=> new User { 
                
                Username = u.Username,
                UserId = u.UserID,
                Email = u.Email,
                SignUpDate = u.SignupDate
                }).FirstOrDefault()

            }).ToList();

            return answers;
        }
        [HttpPost]
        public void PostAnswer(string content, int postId)
        {
            var ans = new DB.Answers();
            var user = GetUser();
            ans.UserId = user.UserId;
            ans.Approved = false;
            ans.Content = content;
            ans.Created = DateTime.Now;
            ans.PostId = postId;
            db.Answers.Add(ans);
            db.SaveChanges();   
        }
        [HttpPost]
        public void MarkAsApproved(int answerId)
        {

            var answer = db.Answers.FirstOrDefault(a => a.AnswerId == answerId);
            var post = db.Posts.FirstOrDefault(p => p.PostId == answer.PostId);
            var postCreator = db.Users.FirstOrDefault(u => u.UserID == post.OwnerId);
            var user = GetUser();

            if (user.UserId == postCreator.UserID)
            {
                answer.Approved = !answer.Approved;
                var ans = db.Answers.Where(a => a.PostId == post.PostId && a.AnswerId != answerId).ToList();
                foreach (var a in ans)
                {
                    a.Approved = false;
                }

                db.SaveChanges();
            }

        }
        [HttpGet]
        public User GetUser()
        {
            var prinicpal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var email = prinicpal.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
            return db.Users.Where(u => u.Email == email).Select(s => new User { 
            
                UserId = s.UserID,
                Username = s.Username,
                Email = s.Email,
                SignUpDate = s.SignupDate
            }).FirstOrDefault();
        }
    }
}
